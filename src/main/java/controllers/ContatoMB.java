package controllers;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;

import dao.ContatoDAO;
import dominio.Contato;

@ManagedBean
@SessionScoped
public class ContatoMB {
	private Contato contato;
	
	@Inject
	private ContatoDAO contatoDAO;
	
	private List<Contato> listaContatos;
	
	
	public ContatoMB() {
		contato = new Contato();
		listaContatos = new ArrayList<Contato>(); 
	}
	
	public Contato getContato() {
		return contato;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}
	
	public List<Contato> getListaContatos() {
		setListaContatos(contatoDAO.listar());
		return listaContatos;
	}

	public void setListaContatos(List<Contato> listaContatos) {
		this.listaContatos = listaContatos;
	}

	public String cadastrar() {
		Contato c = contatoDAO.buscarContatoNome(contato.getNome());
		if (c == null) {
			contatoDAO.salvar(contato);
		} else {
			contatoDAO.atualizar(contato);
		}
		contato = new Contato();
		return "/interna/lista.jsf";
	}
	
	public String novo() {
		return "/interna/cadastra.jsf";
	}
}
